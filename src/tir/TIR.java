/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tir;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jamil
 */
public class TIR {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        try {
        List<Double> flujo = new ArrayList<>();
        for(String x : args) {
            flujo.add(Double.parseDouble(x));            
        }
        System.out.println(CalculadorTir.aproximarTIR(flujo));
        }
        catch (Exception e){
            System.out.println("Ocurrió un error!" + e.getMessage());
        }
    }   
}
