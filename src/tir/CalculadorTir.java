/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tir;

import java.util.List;

/**
 *
 * @author Jamil
 */
public class CalculadorTir {

    public static double aproximarTIR(List<Double> flujo) {
        double total = 1;
        double r = 0;
        double rMax = 100;
        double rMin = -100;
        
        //Busqueda binaria
        while (total < -0.0000001 || total > 0.0000001) {
            r = (rMax + rMin) / 2;
            total = calculoSumado(flujo,r);
            if (total > 0.0000001) {
                rMin = r;
            }
            if (total < -0.0000001) {
                rMax = r;
            }
            
        }
        double x = total;
        return r;
    }
    
    private static double calculoSumado(List<Double> flujo, double r) {
        double total = 0;        
        int exp = 0;
        for (Double flujoIndividual : flujo) {
            total += calculoUnFlujo(flujoIndividual, r, exp);
            exp++;
        }
        return total;
    }
    
    private static double calculoUnFlujo(double flujo, double r, int numeroPeriodo) {
        return flujo/(Math.pow(1+r, numeroPeriodo));
    }    
}
