/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tir;

import java.util.ArrayList;
import java.util.List;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author Jamil
 */
public class CalculadorTirNGTest {
    
    public CalculadorTirNGTest() {
    }

    /**
     * Test of aproximarTIR method, of class CalculadorTir.
     */
    @Test(dataProvider = "aproximarTir")
    public void testAproximarTIR(List<Double> flujo, double esperado) {
        double result = CalculadorTir.aproximarTIR(flujo);
        assertEquals(result, esperado ,0.0000001);
    }
    
    private List<Double> convertToList(double[] list) {
        ArrayList<Double> x = new ArrayList<>();
        for (double y : list) {
            x.add(y);            
        }
        return x;
    }

    @DataProvider(name = "aproximarTir")
    public Object[][] provideData() {        
        return new Object[][] {
            {convertToList(new double[]{-123400.0,36200.0,54800.0,48100.0}),0.0596163785673296},
            {convertToList(new double[]{-300000,150000,150000,150000,10000}),0.2431185387386130},
            {convertToList(new double[]{-110000000.0,22000000.0,24200000.0,26620000.0,29282000.0,82210200.0}),0.157851140378779},
            {convertToList(new double[]{-600000,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,12596.97
                                               ,12596.97,12596.97,12596.97,12596.97,162596.97}),0.01253806283194}
        };
    }
}
